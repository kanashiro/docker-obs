== Build from scratch: ==

1. Minbase image
   If you are using Debian based system, a minimal base image can be built
   using `docker.io` provided tools with:

   ```
   $ sudo make minbase
   ```

   That command creates `obs/minbase` image, the rest of containers are built
   on top of this image.

2. Build images for OBS server and OBS api to run on different containers
   - OBS server container image
   - OBS api container image
   - OBS worker container image

   To build that images and launch in multiple containers with compose, execute:

   ```
   $ sudo make
   ```
== Run containers ==

1. Run the containers
   ```
   $ sudo docker-compose up
   $ sudo docker-compose scale obs-worker=2
   ```
== Access Open Build Service UI ==

1. Open Build Service should be now running, point browser to
   `https://localhost/`

== Troubleshooting ==

1. docker compose fails to start
   See https://github.com/docker/compose/issues/1113
   Some cleanup might be needed if you are doing tests.
   $ docker ps -a
   $ docker rm CONTAINER_ID (listed by previous command)

== obs-tests/ ==

1. The obs-tests/ directory contains documentations, images and scripts
for testing. Please read more details in obs-tests/README.md.

As a primer on Docker:
======================

* build it
`docker build --tag my-image-name:tag .`

* run it
`docker run --name my-container-name -d my-image-name:tag`

* check the logs
`docker logs my-container-name`

* list running containers
`docker ps`

* list all containers
`docker ps -a`

more info on docker commands:
* https://github.com/wsargent/docker-cheat-sheet
